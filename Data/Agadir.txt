/*------------ Agadir(1) -> Rabat(2) --------------*/
/***********SUPRATOURS**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(212,2,3,1,2,'00:30:00','09:15:00','false',220 ,2 ),
(213,2,3,1,2,'02:00:00','11:15:00','false',220 ,2 ),
(214,2,3,1,2,'02:30:00','13:15:00','false',220 ,2 ),
(215,2,3,1,2,'05:00:00','13:15:00','false',220 ,2 ),
(216,2,3,1,2,'06:00:00','15:15:00','false',220 ,2 ),
(217,2,3,1,2,'07:00:00','15:15:00','false',220 ,2 ),
(218,2,3,1,2,'09:00:00','17:15:00','false',220 ,2 ),
(219,2,3,1,2,'11:00:00','19:15:00','false',220 ,2 ),
(220,2,3,1,2,'11:30:00','23:15:00','false',220 ,2 ),
(221,2,3,1,2,'13:00:00','21:15:00','false',220 ,2 ),
(222,2,3,1,2,'15:00:00','23:15:00','false',220 ,2 ),
(223,2,3,1,2,'17:00:00','01:53:00','true',220 , 2);

/***********CTM**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(224,2,2,1,2,'00:30:00','08:30:00','false',250 ,3 ),
(225,2,2,1,2,'01:45:00','11:00:00','false',250 ,3 ),
(226,2,2,1,2,'06:00:00','14:45:00','false',250 ,3 ),
(227,2,2,1,2,'09:45:00','19:15:00','false',250 ,3 ),
(228,2,2,1,2,'22:30:00','07:15:00','true',250 ,3 ); 


/*------------ Agadir(1) -> Marrakech(3) --------------*/
/***********SUPRATOURS**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(229,2,3,1,3,'00:30:00','04:30:00','false',100 ,3 ),
(230,2,3,1,3,'02:00:00','06:30:00','false',100 ,3 ),
(231,2,3,1,3,'02:30:00','08:29:00','false',100 ,3 ),
(232,2,3,1,3,'05:00:00','08:45:00','false',100 ,3 ),
(233,2,3,1,3,'06:00:00','09:59:00','false',100 ,3 ),
(234,2,3,1,3,'07:00:00','10:30:00','false',100 ,3 ),
(235,2,3,1,3,'08:30:00','12:29:00','false',100 ,3 ),
(236,2,3,1,3,'09:00:00','12:45:00','false',100 ,3 ),
(237,2,3,1,3,'11:00:00','14:45:00','false',100 ,3 ),
(238,2,3,1,3,'11:30:00','15:00:00','false',100 ,3 ),
(239,2,3,1,3,'13:00:00','16:45:00','false',100 ,3 ),
(240,2,3,1,3,'15:00:00','18:45:00','false',100 ,3 ),
(241,2,3,1,3,'17:00:00','20:45:00','false',100 ,3 ),
(242,2,3,1,3,'19:30:00','22:59:00','false',100 ,3 );

/***********CTM**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(243,2,2,1,3,'00:30:00','03:30:00','false',105 ,3 ),
(244,2,2,1,3,'01:45:00','05:30:00','false',105 ,3 ),
(245,2,2,1,3,'05:45:00','09:30:00','false',105 ,3 ),
(246,2,2,1,3,'06:00:00','09:15:00','false',105 ,3 ),
(247,2,2,1,3,'08:00:00','11:00:00','false',105 ,3 ),
(248,2,2,1,3,'09:00:00','12:00:00','false',105 ,3 ),
(249,2,2,1,3,'09:45:00','13:15:00','false',105 ,3 ),
(250,2,2,1,3,'18:30:00','21:40:00','false',105 ,3 ),
(251,2,2,1,3,'13:00:00','16:30:00','false',105 ,3 ),
(252,2,2,1,3,'15:30:00','19:15:00','false',105 ,3 ),
(253,2,2,1,3,'16:00:00','20:00:00','false',105 ,3 ),
(254,2,2,1,3,'22:30:00','02:00:00','true',105 ,3 );



/*------------ Agadir(1) -> Casablanca(4) --------------*/
/***********SUPRATOURS**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(255,2,3,1,4,'00:30:00','08:10:00','false',190 ,2 ),
(256,2,3,1,4,'02:00:00','10:10:00','false',190 ,2 ),
(257,2,3,1,4,'02:30:00','12:10:00','false',190 ,2 ),
(258,2,3,1,4,'05:00:00','12:10:00','false',190 ,2 ),
(259,2,3,1,4,'06:00:00','14:10:00','false',190 ,2 ),
(260,2,3,1,4,'07:00:00','14:10:00','false',190 ,2 ),
(261,2,3,1,4,'08:30:00','21:10:00','false',190 ,2 ),
(262,2,3,1,4,'09:00:00','16:10:00','false',190 ,2 ),
(263,2,3,1,4,'11:00:00','18:10:00','false',190 ,2 ),
(264,2,3,1,4,'11:30:00','22:10:00','false',190 ,2 ),
(265,2,3,1,4,'13:00:00','20:10:00','false',190 ,2 ),
(266,2,3,1,4,'15:00:00','20:10:00','false',190 ,2 ),
(267,2,3,1,4,'17:00:00','00:10:00','true',190 , 2);

/***********CTM**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(268,2,2,1,4,'00:30:00','07:05:00','false',205 ,3 ),
(269,2,2,1,4,'01:45:00','09:10:00','false',205 ,3 ),
(270,2,2,1,4,'08:00:00','15:30:00','false',205 ,3 ),
(271,2,2,1,4,'09:00:00','16:30:00','false',205 ,3 ),
(272,2,2,1,4,'09:45:00','17:35:00','false',205 ,3 ),
(273,2,2,1,4,'18:30:00','01:00:00','false',205 ,3 ),
(274,2,2,1,4,'11:00:00','17:30:00','false',260 ,3 ),
(275,2,2,1,4,'13:00:00','20:45:00','false',205 ,3 ),
(276,2,2,1,4,'15:30:00','23:00:00','false',205 ,3 ),
(277,2,2,1,4,'22:30:00','05:45:00','true',205 ,3 ),
(278,2,2,1,4,'23:59:00','06:00:00','true',260 ,3 );


/*------------ Agadir(1) -> Tanger(5) --------------*/
/***********SUPRATOURS**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(279,2,3,1,5,'00:30:00','14:30:00','false',305 ,2 ),
(280,2,3,1,5,'02:00:00','16:30:00','false',305 ,2 ),
(281,2,3,1,5,'02:30:00','18:30:00','false',305 ,2 ),
(282,2,3,1,5,'05:00:00','18:30:00','false',305 ,2 ),
(283,2,3,1,5,'06:00:00','20:20:00','false',305 ,2 ),
(284,2,3,1,5,'07:00:00','20:20:00','false',305 ,2 ),
(285,2,3,1,5,'09:00:00','22:25:00','false',305 ,2 ),
(286,2,3,1,5,'17:00:00','07:00:00','true',305 , 2);

/***********CTM**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(287,2,2,1,5,'22:30:00','17:00:00','true',335 ,3 ); 


/*------------ Agadir(1) -> Fes(6) --------------*/
/***********SUPRATOURS**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(288,2,3,1,6,'00:30:00','12:10:00','false',295 ,3 ),
(289,2,3,1,6,'02:00:00','14:10:00','false',295 ,3 ),
(290,2,3,1,6,'02:30:00','16:10:00','false',295 ,3 ),
(291,2,3,1,6,'05:00:00','16:10:00','false',295 ,3 ),
(292,2,3,1,6,'06:00:00','18:10:00','false',295 ,3 ),
(293,2,3,1,6,'07:00:00','18:10:00','false',295 ,3 ),
(294,2,3,1,6,'09:00:00','20:10:00','false',295 ,3 ),
(295,2,3,1,6,'11:00:00','22:10:00','false',295 ,3 ),
(296,2,3,1,6,'11:30:00','22:10:00','false',295 ,3 ),
(297,2,3,1,6,'13:00:00','00:10:00','true',295 ,3 ),
(298,2,3,1,6,'15:00:00','02:10:00','true',295 ,3 );


/***********CTM**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(299,2,2,1,6,'06:00:00','18:30:00','false',275 ,3 ),
(300,2,2,1,6,'22:30:00','11:00:00','true',260 ,3 );


/*------------ Agadir(1) -> Meknes(7) --------------*/
/***********SUPRATOURS**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(301,2,3,1,7,'00:30:00','11:32:00','false',274 ,2 ),
(302,2,3,1,7,'02:00:00','13:32:00','false',274 ,2 ),
(303,2,3,1,7,'02:30:00','15:32:00','false',274 ,2 ),
(304,2,3,1,7,'05:00:00','15:32:00','false',274 ,2 ),
(305,2,3,1,7,'06:00:00','17:32:00','false',274 ,2 ),
(306,2,3,1,7,'07:00:00','17:32:00','false',274 ,2 ),
(307,2,3,1,7,'09:00:00','19:32:00','false',274 ,2 ),
(308,2,3,1,7,'11:00:00','21:32:00','false',274 ,2 ),
(309,2,3,1,7,'11:30:00','01:29:00','true',274 , 2),
(310,2,3,1,7,'13:00:00','23:29:00','false',274 ,2 ),
(311,2,3,1,7,'15:00:00','01:29:00','true',274 , 2),
(312,2,3,1,7,'19:30:00','06:59:00','true',262 ,3 );

/***********CTM**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(313,2,2,1,7,'06:00:00','17:25:00','false',265 ,3 );


/*------------ Agadir(1) -> Ifrane(8) --------------*/
/***********CTM**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(314,2,2,1,8,'22:30:00','09:50:00','true',250 ,3 );


/*------------ Agadir(1) -> Beni Mellal(9) --------------*/
/***********SUPRATOURS**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(315,2,3,1,9,'08:30:00','16:29:00','false',160 ,3 ),
(316,2,3,1,9,'19:30:00','02:29:00','false',160 ,3 );

/***********CTM**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(317,2,2,1,9,'22:30:00','05:50:00','true',165 ,3 ),
(318,2,2,1,9,'23:00:00','05:30:00','true',170 ,3 );


/*------------ Agadir(1) -> Boujdour(10) --------------*/
/***********SUPRATOURS**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(319,2,3,1,10,'10:00:00','22:59:00','false',295 ,3 ),
(320,2,3,1,10,'19:00:00','10:29:00','true',295 ,3 ),
(320,2,3,1,10,'21:00:00','11:59:00','true',295 ,3 );

/***********CTM**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(321,2,2,1,10,'10:00:00','00:25:00','true',300 ,3 ),
(322,2,2,1,10,'20:45:00','10:00:00','true',300 ,3 ),
(323,2,2,1,10,'21:45:00','11:15:00','true',300 ,3 ),
(324,2,2,1,10,'22:15:00','12:15:00','true',285 ,3 );


/*------------ Agadir(1) -> Dakhla(11) --------------*/
/***********CTM**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(325,2,2,1,11,'10:00:00','05:15:00','true',390 ,3 ),
(326,2,2,1,11,'20:45:00','15:30:00','true',390 ,3 ),
(327,2,2,1,11,'21:45:00','17:00:00','true',390 ,3 ),
(328,2,2,1,11,'22:15:00','17:30:00','true',370 ,3 );


/*------------ Agadir(1) -> Eljadida(12) --------------*/
/***********CTM**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(329,2,2,1,12,'12:30:00','20:20:00','false',170 ,3 ),
(330,2,2,1,12,'08:00:00','16:45:00','false',170 ,3 );


/***********SUPRATOURS**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(331,2,3,1,12,'00:30:00','09:50:00','false',225 ,2 ),
(332,2,3,1,12,'02:00:00','11:50:00','false',225 ,2 ),
(333,2,3,1,12,'02:30:00','13:50:00','false',225 ,2 ),
(334,2,3,1,12,'05:00:00','13:50:00','false',225 ,2 ),
(335,2,3,1,12,'06:00:00','15:50:00','false',225 ,2 ),
(336,2,3,1,12,'07:00:00','15:50:00','false',225 ,2 ),
(337,2,3,1,12,'09:00:00','18:50:00','false',225 ,2 ),
(338,2,3,1,12,'11:00:00','20:50:00','false',225 ,2 ); 


/*------------ Agadir(1) -> Essaouira(14) --------------*/
/***********SUPRATOURS**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(339,2,3,1,14,'09:00:00','12:44:00','false',65 ,3 );

/***********CTM**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(340,2,2,1,14,'12:30:00','16:05:00','false',75 ,3 ),
(341,2,2,1,14,'08:00:00','11:20:00','false',75 ,3 );



/*------------ Agadir(1) -> Kenitra(15) --------------*/
/***********SUPRATOURS**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(342,2,3,1,15,'00:30:00','09:60:00','false',235 ,2 ),
(343,2,3,1,15,'02:00:00','11:46:00','false',235 ,2 ),
(344,2,3,1,15,'02:30:00','13:46:00','false',235 ,2 ),
(345,2,3,1,15,'05:00:00','13:46:00','false',235 ,2 ),
(346,2,3,1,15,'06:00:00','15:46:00','false',235 ,2 ),
(347,2,3,1,15,'07:00:00','15:46:00','false',235 ,2 ),
(348,2,3,1,15,'09:00:00','17:46:00','false',235 ,2 ),
(349,2,3,1,15,'11:00:00','19:46:00','false',235 ,2 ),
(350,2,3,1,15,'11:30:00','23:49:00','false',235 ,2 ),
(351,2,3,1,15,'13:00:00','21:46:00','false',235 ,2 ),
(352,2,3,1,15,'15:00:00','23:49:00','false',235 ,2 ),
(353,2,3,1,15,'17:00:00','02:29:00','true',235 , 2);


/*------------ Agadir(1) -> Khenifra(16) --------------*/
/***********CTM**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(354,2,2,1,16,'22:30:00','08:00:00','true',220 ,3 );


/*------------ Agadir(1) -> Khouribga(17) --------------*/
/***********SUPRATOURS**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(355,2,3,1,17,'08:30:00','18:35:00','false',187 ,3 );


/*------------ Agadir(1) -> Laayoune(18) --------------*/
/***********SUPRATOURS**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(356,2,3,1,18,'10:00:00','19:59:00','false',230 ,3 ),
(357,2,3,1,18,'19:00:00','06:59:00','true',230 ,3 ),
(358,2,3,1,18,'20:30:00','06:59:00','true',230 ,3 ),
(359,2,3,1,18,'21:00:00','09:29:00','true',230 ,3 );

/***********CTM**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(360,2,2,1,18,'00:40:00','11:30:00','false',240 ,3 ),
(361,2,2,1,18,'07:30:00','17:00:00','false',240 ,3 ),
(362,2,2,1,18,'10:00:00','21:40:00','false',240 ,3 ),
(363,2,2,1,18,'18:00:00','05:15:00','true',240 ,3 ),
(364,2,2,1,18,'20:45:00','07:30:00','true',240 ,3 ),
(365,2,2,1,18,'21:45:00','08:15:00','true',240 ,3 ),
(366,2,2,1,18,'22:15:00','19:15:00','true',230 ,3 ),
(367,2,2,1,18,'22:30:00','10:00:00','true',240 ,3 );



/*------------ Agadir(1) -> Nador(19) --------------*/
/***********SUPRATOURS**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(368,2,3,1,19,'02:30:00','23:12:00','false',348 ,2 ),
(369,2,3,1,19,'05:00:00','23:12:00','false',348 ,2 ),
(370,2,3,1,19,'11:00:00','05:57:00','true',348 , 2),
(371,2,3,1,19,'11:30:00','09:32:00','true',348 , 2),
(372,2,3,1,19,'15:00:00','09:32:00','true',348 , 2),
(373,2,3,1,19,'19:30:00','17:02:00','true',348 , 2);


/*------------ Agadir(1) -> Ouda(20) --------------*/
/***********SUPRATOURS**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(374,2,3,1,20,'02:00:00','21:00:00','false',380 ,2 ),
(375,2,3,1,20,'11:30:00','08:10:00','true',380 ,2 ),
(376,2,3,1,20,'15:00:00','08:10:00','true',380 ,2 ),
(377,2,3,1,20,'19:30:00','16:35:00','true',380 ,2 );


/*------------ Agadir(1) -> Safi(22) --------------*/
/***********SUPRATOURS**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(378,2,3,1,22,'09:00:00','15:00:00','false',100 ,3 );

/***********CTM**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(379,2,2,1,22,'12:30:00','18:05:00','false',115 ,3 ),
(380,2,2,1,22,'08:00:00','14:15:00','false',115 ,3 );


/*------------ Agadir(1) -> Samra(23) --------------*/
/***********SUPRATOURS**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(381,2,3,1,23,'20:30:00','06:00:00','true',170 ,3 );

/***********CTM**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(382,2,2,1,23,'20:15:00','06:30:00','true',170 ,3 );



/*------------ Agadir(1) -> Tetouna(24) --------------*/
/***********SUPRATOURS**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(383,2,3,1,24,'02:00:00','17:44:00','false',352 ,2 ),
(384,2,3,1,24,'09:00:00','23:45:00','false',352 ,2 ),
(385,2,3,1,24,'17:00:00','08:44:00','true',352  ,2 );



/*------------ Agadir(1) -> Dakhla(11) suite--------------*/
/***********SUPRATOURS**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(386,2,3,1,11,'10:00:00','05:59:00','true',380 ,3 ),
(387,2,3,1,11,'19:00:00','15:00:00','true',380 ,3 ),
(388,2,3,1,11,'21:00:00','17:00:00','true',380 ,3 );

















