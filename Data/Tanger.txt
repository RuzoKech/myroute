/*------------ Tanger(5) -> Agadir(1) --------------*/
/***********ONCF**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(1000,1,1,5,1,'05:35:00','18:29:00','false',305 ,2 ),
(1001,1,1,5,1,'07:35:00','20:00:00','false',355 ,2 ),
(1002,1,1,5,1,'08:25:00','22:29:00','false',305 ,2 ),
(1003,1,1,5,1,'09:35:00','22:29:00','false',305 ,2 ),
(1004,1,1,5,1,'10:40:00','00:31:00','true',305 ,2 ),
(1005,1,1,5,1,'11:35:00','00:31:00','true',305 ,2 ),
(1006,1,1,5,1,'13:05:00','02:29:00','true',305 ,2 ),
(1007,1,1,5,1,'15:35:00','04:29:00','true',305 ,2 ),
(1008,1,1,5,1,'21:35:00','11:59:00','true',305 ,2 );


/*------------ Tanger(5) -> Rabat(2) --------------*/
/***********ONCF**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(1009,1,1,5,2,'05:35:00','09:10:00','false',95 ,2 ),
(1010,1,1,5,2,'07:35:00','11:09:00','false',95 ,2 ),
(1011,1,1,5,2,'08:25:00','13:42:00','false',95 ,2 ),
(1012,1,1,5,2,'09:35:00','13:09:00','false',95 ,2 ),
(1013,1,1,5,2,'10:40:00','15:42:00','false',95 ,2 ),
(1014,1,1,5,2,'11:35:00','15:09:00','false',95 ,2 ),
(1015,1,1,5,2,'13:35:00','17:09:00','false',95 ,2 ),
(1016,1,1,5,2,'15:35:00','19:09:00','false',95 ,2 ),
(1017,1,1,5,2,'17:35:00','21:09:00','false',95 ,2 ),
(1018,1,1,5,2,'21:35:00','03:07:00','true',95 ,2 );

/***********CTM**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(1019,2,2,5,2,'11:00:00','14:45:00','false',110 ,3 ),
(1020,2,2,5,2,'14:00:00','17:50:00','false',110 ,3 ),
(1021,2,2,5,2,'23:45:00','03:30:00','true',110 ,3 );


/*------------ Tanger(5) -> Marrakech(3) --------------*/
/***********ONCF**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(1022,1,1,5,3,'05:35:00','14:00:00','false',95 ,2 ),
(1023,1,1,5,3,'07:35:00','16:00:00','false',95 ,2 ),
(1024,1,1,5,3,'08:25:00','18:00:00','false',95 ,2 ),
(1025,1,1,5,3,'09:35:00','18:00:00','false',95 ,2 ),
(1026,1,1,5,3,'10:40:00','20:00:00','false',95 ,2 ),
(1027,1,1,5,3,'11:35:00','20:00:00','false',95 ,2 ),
(1028,1,1,5,3,'13:35:00','22:00:00','false',95 ,2 ),
(1029,1,1,5,3,'15:35:00','23:59:00','false',95 ,2 ),
(1030,1,1,5,3,'17:35:00','23:59:00','false',95 ,2 ),
(1031,1,1,5,3,'21:35:00','08:00:00','true',95 ,2 );


/*------------ Tanger(5) -> Casablanca(4) --------------*/
/***********ONCF**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(1032,1,1,5,4,'05:35:00','10:20:00','false',125 ,2 ),
(1033,1,1,5,4,'07:35:00','12:20:00','false',125 ,2 ),
(1034,1,1,5,4,'08:25:00','14:45:00','false',125 ,2 ),
(1035,1,1,5,4,'09:35:00','14:20:00','false',125 ,2 ),
(1036,1,1,5,4,'10:40:00','16:45:00','false',125 ,2 ),
(1037,1,1,5,4,'11:35:00','16:20:00','false',125 ,2 ),
(1038,1,1,5,4,'13:35:00','18:20:00','false',125 ,2 ),
(1039,1,1,5,4,'15:35:00','20:20:00','false',125 ,2 ),
(1040,1,1,5,4,'17:35:00','22:20:00','false',125 ,2 ),
(1041,1,1,5,4,'21:35:00','04:30:00','true',125 ,2 );

/***********CTM**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(1042,2,2,5,4,'11:00:00','16:30:00','false',145 ,3 ),
(1043,2,2,5,4,'14:00:00','20:20:00','false',140 ,3 ),
(1044,2,2,5,4,'23:45:00','05:15:00','true',145 ,3 );


/*------------ Tanger(5) -> Fes(6) --------------*/
/***********ONCF**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(1045,1,1,5,6,'08:25:00','13:00:00','false',105 ,2 ),
(1046,1,1,5,6,'10:40:00','15:00:00','false',105 ,2 ),
(1047,1,1,5,6,'13:05:00','17:35:00','false',105 ,2 ),
(1048,1,1,5,6,'17:35:00','21:50:00','false',105 ,2 ),
(1049,1,1,5,6,'21:35:00','02:10:00','true',105 ,2 );

/***********CTM**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(1050,2,2,5,6,'12:15:00','18:30:00','false',115 ,3 ),
(1051,2,2,5,6,'15:00:00','21:00:00','false',115 ,3 ),
(1052,2,2,5,6,'21:30:00','05:15:00','false',115 ,3 ),
(1053,2,2,5,6,'22:00:00','03:50:00','false',115 ,3 ),
(1054,2,2,5,6,'23:45:00','05:00:00','false',115 ,3 );



/*------------ Tanger(5) -> Meknes(7) --------------*/
/***********ONCF**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(1055,1,1,5,7,'08:25:00','12:19:00','false',85 ,2 ),
(1056,1,1,5,7,'10:40:00','14:19:00','false',85 ,2 ),
(1057,1,1,5,7,'13:05:00','16:55:00','false',85 ,2 ),
(1058,1,1,5,7,'17:35:00','21:09:00','false',85 ,2 ),
(1059,1,1,5,7,'21:35:00','01:29:00','true',85 ,2 );

/***********CTM**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(1060,2,2,5,7,'15:00:00','19:55:00','false',90 ,3 ),
(1061,2,2,5,7,'21:30:00','04:05:00','false',90 ,3 ),
(1062,2,2,5,7,'22:00:00','03:00:00','true',90 ,3 ),
(1063,2,2,5,7,'23:45:00','03:55:00','true',90 ,3 );


/*------------ Tanger(5) -> BeniMellal(9) --------------*/
/***********ONCF**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(1064,1,1,5,9,'07:35:00','17:30:00','false',195 ,2 );


/*------------ Tanger(5) -> Kenitra(15) --------------*/
/***********ONCF**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(1065,1,1,5,15,'05:35:00','08:36:00','false',85 ,2 ),
(1066,1,1,5,15,'07:35:00','10:35:00','false',85 ,2 ),
(1067,1,1,5,15,'08:25:00','13:06:00','false',85 ,2 ),
(1068,1,1,5,15,'09:35:00','12:35:00','false',85 ,2 ),
(1069,1,1,5,15,'10:40:00','15:06:00','false',85 ,2 ),
(1070,1,1,5,15,'11:35:00','14:35:00','false',85 ,2 ),
(1071,1,1,5,15,'13:35:00','16:35:00','false',85 ,2 ),
(1072,1,1,5,15,'15:35:00','18:35:00','false',85 ,2 ),
(1073,1,1,5,15,'17:35:00','20:32:00','false',85 ,2 ),
(1074,1,1,5,15,'21:35:00','02:10:00','true',85 ,2 );


/*------------ Tanger(5) -> Nador(19) --------------*/
/***********ONCF**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(1075,1,1,5,19,'21:35:00','09:32:00','true',183 ,2 );

/***********CTM**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(1076,2,2,5,19,'22:00:00','08:00:00','true',195 ,3 );


/*------------ Tanger(5) -> Oujda(20) --------------*/
/***********ONCF**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(1077,1,1,5,20,'10:40:00','21:00:00','false',210 ,2 ),
(1078,1,1,5,20,'21:35:00','08:10:00','true',210 ,2 );

/***********CTM**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(1079,2,2,5,20,'21:30:00','10:00:00','true',200 ,3 );


/*------------ Tanger(5) -> Tetouan(24) --------------*/
/***********CTM**********/
INSERT INTO voyages (id,type_id,societe_id,ville_depart,ville_arrive,depart_time,arrive_time,nextday,tarif,classe) VALUES
(1080,2,2,5,24,'08:15:00','09:00:00','false',15 ,3 ),
(1081,2,2,5,24,'12:15:00','13:30:00','false',20 ,3 ),
(1082,2,2,5,24,'16:45:00','17:30:00','false',20 ,3 ),
(1083,2,2,5,24,'21:15:00','22:15:00','false',20 ,3 );









