<div id="container">
	<div id="searchheader"></div>
	<div id="postdepdes"><p><?php echo $ville_dep['Ville']['nville']."&nbsp;&nbsp;&nbsp;>>>&nbsp;&nbsp;&nbsp;".$ville_des['Ville']['nville'];?></p></div>
	<ul id="postscontainer" class="js-masonry grid effect-2"  data-masonry-options='{ "columnWidth": 20, "itemSelector": ".postcontainer" }'>
		
		<li class="postcontainer"  id="searchcontainer" >
			
			<?php
				$societesMaroc[0] = 'ALL'; 
				ksort($societesMaroc);
				echo $this->Form->create('voyages',array('action'=>'search'));
				echo $this->Form->input('depart',array('options'=>$villesMaroc));
				echo $this->Form->input('destination',array('options'=>$villesMaroc));
				echo $this->Form->input('depart_time',array('type' => 'time', 'interval' => 15,'label'=>'Quitte Apres',
				'selected' => array('hour' => '12','minute' => '00','meridian' => 'pm')));
				
				echo $this->Form->input('societe',array('options'=>array($societesMaroc)));
				
				echo $this->Form->end(__('consulter'));
			?>
			<div id="postsociete"><?php echo 'Recherche Rapide'; ?></div>
		</li>
		
		<li class="postcontainer"  style="cursor: pointer; margin-bottom: 20px; min-height:90px;">			
		<div class="fb-like facebooklike" data-href="https://facebook.com/maroute.ma" data-width="280" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
		<div id="postsociete" style="background-color:#4865b5;font-size:15px;"><a href="http://www.facebook.com/maroute.ma" target="_blank"><?php echo 'Partager sur Facebook'; ?></a></div>
		</li>
		
		<?php if(!$voyageSearch){ ?>
			<li class="postcontainer"  style=" width:500px; ">
				<div id="postmessage"><a href="http://www.facebook.com/maroute.ma" target="_blank"><p>Le trajet demandé n'est pas attribué actuellement sur nos annuaires. 
					Veuillez indiquer ce trajet sur votre page <font>facebook/maroute.ma</font> afin que nous fassions suite a votre demande.</p></a></div>
				<div id="postsociete" style="background-color:#13990d;"><a href="http://www.facebook.com/maroute.ma" target="_blank"><?php echo 'Oups! C\'est embarassant'; ?></a></div>
				
				<!--<div id="postmessage"><p>Le trajet demandé n'est pas attribué actuellement sur nos annuaires. 
					Veuillez indiquer toutes remarques sur votre page <font>facebook/maroute.ma</font> </p></div>
				<div id="postsociete" style="background-color:#C43C35;"><?php echo 'Merci pour votre compréhension'; ?></div>-->
			</li>
		<?php }else{?>
		<?php foreach ($voyageSearch as $voyage): ?>
			<li class="postcontainer"   >
				
				
				<?php if(file_exists( WWW_ROOT.'/img/'.strtolower($voyage['Societe']['nsociete'].'.png'))) {?>
				<div id="postlogo"><?php echo $this->Html->image(strtolower($voyage['Societe']['nsociete'].'.png'));?></div>
				<?php }?>
				
				<table>
					<tr>
						<td><?php echo 'Départ <br><font>'.substr($voyage['Voyage']['depart_time'],0,5).'</font>'?></td>
						<?php $nextday = ($voyage['Voyage']['nextday'] == true)? "le lendemain" : ""; ?>
						<td><?php echo 'Arrivée <br><font>'.substr($voyage['Voyage']['arrive_time'],0,5).'</font><br>'.$nextday;?></td>
					</tr>
				</table>
				
				<div id="posttarif"><?php echo h($voyage['Voyage']['tarif']).'  <font>Dh</font>'; ?></div>
				
				<?php 
					for($i=0;$i<$voyage['Voyage']['classe'];$i++)
						echo $this->Html->image('star_logo.png');
				?>
				
				<div id="posttype"><?php echo h($voyage['Type']['ntype']); ?></div>
				<div id="postsociete"><?php echo h($voyage['Societe']['nsociete']); ?></div>
			</li>
		<?php endforeach; ?>
		<?php }?>
		

	</ul>
</div>
<script>
			new AnimOnScroll( document.getElementById( 'postscontainer' ), {
				minDuration : 0.4,
				maxDuration : 0.7,
				viewportFactor : 0.2
			} );
</script>
<script>//$(window).load(function(){   $('#container').masonry(); });</script>