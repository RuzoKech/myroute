<div class="voyages view">
<h2><?php echo __('Voyage'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($voyage['Voyage']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type'); ?></dt>
		<dd>
			<?php echo $this->Html->link($voyage['Type']['ntype'], array('controller' => 'types', 'action' => 'view', $voyage['Type']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Societe'); ?></dt>
		<dd>
			<?php echo $this->Html->link($voyage['Societe']['nsociete'], array('controller' => 'societes', 'action' => 'view', $voyage['Societe']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ville  Depart'); ?></dt>
		<dd>
			<?php echo $this->Html->link($voyage['Ville_Depart']['nville'], array('controller' => 'villes', 'action' => 'view', $voyage['Ville_Depart']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ville  Arrive'); ?></dt>
		<dd>
			<?php echo $this->Html->link($voyage['Ville_Arrive']['nville'], array('controller' => 'villes', 'action' => 'view', $voyage['Ville_Arrive']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Depart Time'); ?></dt>
		<dd>
			<?php echo h($voyage['Voyage']['depart_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Arrive Time'); ?></dt>
		<dd>
			<?php echo h($voyage['Voyage']['arrive_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nextday'); ?></dt>
		<dd>
			<?php echo h($voyage['Voyage']['nextday']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tarif'); ?></dt>
		<dd>
			<?php echo h($voyage['Voyage']['tarif']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Classe'); ?></dt>
		<dd>
			<?php echo h($voyage['Voyage']['classe']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Menu'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Voyage'), array('action' => 'edit', $voyage['Voyage']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Voyage'), array('action' => 'delete', $voyage['Voyage']['id']), null, __('Are you sure you want to delete # %s?', $voyage['Voyage']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Voyages'), array('action' => 'index')); ?> </li>
		<!--<li><?php echo $this->Html->link(__('New Voyage'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Types'), array('controller' => 'types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Type'), array('controller' => 'types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Societes'), array('controller' => 'societes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Societe'), array('controller' => 'societes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Villes'), array('controller' => 'villes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ville  Depart'), array('controller' => 'villes', 'action' => 'add')); ?> </li>-->
	</ul>
</div>
