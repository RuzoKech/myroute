<?php
/**
 *
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 */

$cakeDescription = __d('maroute', 'maroute:  Annuaire des moyens de transports au Maroc');
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php //echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->script('masonry.pkgd.min.js');
		echo $this->Html->script('jquery-1.11.1.min.js');
		echo $this->Html->script('script');
		echo $this->Html->script('classie');
		echo $this->Html->script('modernizr.custom');			
		echo $this->Html->script('AnimOnScroll');
		echo $this->Html->script('imagesloaded');
							
		echo $this->Html->css('cake.generic');
		echo $this->Html->css('component');
		echo $this->Html->css('cake.maroute');
		echo $this->Html->css('bootstrap.min');
		echo $this->Html->css('style');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>
	<script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '467859646683838',
          xfbml      : true,
          version    : 'v2.0'
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    </script>
   
    
	<div id="header">
		<div id="headercontent">
			<?php echo $this->Html->image('maroute_logo.jpg',array('id'=>'maroutelogo','url'=>'/'));?>
			<?php //echo $this->Html->image('contactLogo.jpg',array('id'=>'contactlogo'));?>
			<?php echo $this->Html->link($this->Html->image('googleLogo.jpg',array('id'=>'googlelogo')),'https://plus.google.com/u/0/b/118137052039916097859/118137052039916097859/posts',array('target'=>'_blank', 'escape' => false) );?>
			<?php echo $this->Html->link($this->Html->image('facebookLogo.jpg',array('id'=>'facebooklogo')),'http://www.facebook.com/maroute.ma',array('target'=>'_blank', 'escape' => false) );?>
		</div>
	</div>
	

			
	
				<?php echo $this->Session->flash(); ?>
	
				<?php echo $this->fetch('content'); ?>
					
		<div id="footer">
			<?php echo $this->Html->image('mrfooter.jpg',array('id'=>'maroutefooter'));?>
		</div>
	<?php //echo $this->element('sql_dump'); ?>
</body>
</html>
