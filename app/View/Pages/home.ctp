<!--<?php echo $this->Html->image('head2.png',array('id'=>'maroutehead'));?>

<div id="homecontent">
	<div id="searchbar">
		<?php 
			echo $this->Form->create('recherche');
			echo $this->Form->input('depart',array('id'=>'departinput'));
			echo $this->Form->input('destination',array('id'=>'destinput'));
			echo $this->Form->end(__('consulter'));
		?>
	</div>	
</div>-->

 <!-- HEADER
	   ================================================== -->	  
	  <header role="navigation" class="visibility animated fadeIn">
		 <div class="container">
			 <div class="row">
				 <div class="col-md-12">
					  <?php echo $this->Html->image('maroute_logo.png');?>
					  <p class="lead"><i>Annuaire</i> des moyens de transports au Maroc</p>
					</div>
				</div>	 
				<div id="homecontent" align="center">
						<?php
							echo $this->Form->create('voyages',array('action'=>'search'));
							//echo $this->Form->create('voyages',array('url'=>'/voyages/search/'.$villesMaroc['depart']) );
							echo $this->Form->input('depart',array('id'=>'departinput','options'=>$villesMaroc));
							echo $this->Form->input('destination',array('id'=>'destinput','options'=>$villesMaroc));
							echo $this->Form->end(__('consulter'));
						?>
				</div> 
			</div>    
		
	 </header>
	  
	  
	  <!-- PURCHASE
	      ================================================== -->
	  <section class="purchase">
		  <div class="container">
			  <div class="row">
				  <div class="col-md-offset-2 col-md-8">
					 <h1>maroute, c'est quoi?</h1>
					 	    <p class="lead">trouvez tout sur cette page ↓</p>
				  </div>
			  </div>
		  </div>
	  </section>

	  <!-- FEATURES
	      ================================================== -->
	  <section class="features">
		  <div class="container">
			  <div class="row">
              <h1>maroute c'est:</h1><br><br>

				  <div class="col-md-4 visibility animated flipInX">
					  <div class="circle">
					  	<?php echo $this->Html->image('bus_logo.png');?>
					  </div>
					  <h2>Global</h2>
					  <p>maroute rassemble toute les moyens de transports au Maroc, en affichant les horaires ainsi que tout les prix.</p>
				  </div>
				
				  <div class="col-md-4 visibility animated flipInX">
					  <div class="circle">
						<?php echo $this->Html->image('train_logo.png');?>
					  </div>
					  <h2>Personel</h2>
					  <p>maroute compare les différents horaires et sociétés de transports afin de faire le meilleurs choix à ses utilisateurs.</p>
				  </div>
				 
				  <div class="col-md-4 visibility animated flipInX">
					  <div class="circle">
					  	<?php echo $this->Html->image('avion_logo.png');?>
					  </div>
					  <h2>Social</h2>
					  <p>maroute ne comptient que les sociétés de transports dont la sureté est une priorité.</p>
				  </div>
				  
			  </div>
		  </div>
	  </section>

