<?php
App::uses('AppModel', 'Model');
/**
 * Ville Model
 *
 */
class Ville extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'nville';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
			'b' => array(
				'rule' => array('b'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'nville' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
	//public $hasMany = 'Voyage';
}
